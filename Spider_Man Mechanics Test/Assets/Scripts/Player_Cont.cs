﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Cont : MonoBehaviour
{
    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public float coolDown = 0.5f;

    private float jumpReset = 0;
    private float timestamp;
    private bool isMoving = false;
    private bool wallCrawling = false;

    private Rigidbody rb;

    public Transform CameraDirection;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (controller.isGrounded)
        {
            jumpReset = 0;
            //speed = 6f;
            Movement();
        }

        if (Input.GetButtonDown("Jump") && jumpReset < 2)
        {
            moveDirection.y = jumpSpeed;
            jumpReset += 1;
        }

        // Apply gravity
        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);

        // Move the controller
        controller.Move(moveDirection * Time.deltaTime);

        if (Input.GetKeyDown("e"))
        {
            timestamp = Time.time + coolDown;
            Debug.Log("timestamp: " + timestamp);
            Debug.Log("Time.time: " + Time.time);
        }


    }


    // Rotates Character based on what they aare touching
    void OnControllerColliderHit(ControllerColliderHit collision)
    {
        if (collision.gameObject.tag == "Level" && timestamp >= Time.time)
        {
            wallCrawling = true;
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(-90, transform.localRotation.y, transform.localRotation.z), Time.deltaTime * 10f);
            CameraDirection.transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(-90, transform.localRotation.y, transform.localRotation.z), Time.deltaTime * 10f);
        }

        if (collision.gameObject.tag == "")
        {
            wallCrawling = false;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, transform.rotation.y, transform.rotation.z), Time.deltaTime * 10f);
            CameraDirection.transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, transform.rotation.y, transform.rotation.z), Time.deltaTime * 10f);
        }
    }
    

    // Movement of player character
    void Movement()
    {
        Vector3 eulerRotation = new Vector3(transform.eulerAngles.x, CameraDirection.transform.eulerAngles.y, transform.eulerAngles.z);

        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection = moveDirection * speed;

        if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }

        if (isMoving)
        {
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(eulerRotation), Time.deltaTime * 10f);
        }
    }
}

//function Update()
//{
//    if ((Input.GetKey("left")) && (Speed < MaxSpeed))
//        Speed = Speed - Acceleration  Time.deltaTime;
//else if ((Input.GetKey("right")) && (Speed > -MaxSpeed))
//        Speed = Speed + Acceleration  Time.deltaTime;
//else
//{
//        if (Speed > Deceleration  Time.deltaTime)
//        Speed = Speed - Deceleration  Time.deltaTime;
//    else if (Speed < -Deceleration  Time.deltaTime)
//        Speed = Speed + Deceleration  Time.deltaTime;
//    else
//        Speed = 0;
//    }


//    transform.position.x = transform.position.x + Speed * Time.deltaTime;
//}